import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;

class PersonenPanel extends JPanel {

	
	 Person mario = new Person("Brothers","Supermario", "mario.png","mario.wav");
	 Person link = new Person ("Master of the Sword ","Link","link.jpg","zelda.wav");
	 Person mozart = new Person("mozart","Amadeus","Mozart.jpg","cmoll.wav");
	 Person ludwig = new Person("ludwig","van Beethoven","Beethoven.jpg","gmoll.wav");
	 
	/**
	 * <pre>
	 * - NullLayout setzen - person1 mit Mozart erzeugen - person2 mit Beethoven
	 * erzeugen - person1 bei Koordinaten (15, 0, 215, 350) hinzuf�gen - person2 bei
	 * Koordinaten (230, 0, 215, 350) hinzuf�gen
	 */
	public PersonenPanel() {
		setLayout(null);
	
		add(mario).setBounds(15, 0,215,350);
		add(link).setBounds(230,0,215,350);
		add(mozart).setBounds(230,430,215,350);
		add(ludwig).setBounds(15,430,215,350);
		
	}
}

public class PersonenPanelFrame extends JFrame {
	private PersonenPanel view;

	public PersonenPanelFrame() {
		view = new PersonenPanel();
		add(view);
		setSize(600, 1200);
		setTitle("Personen Panel");
		setIconImage(Utility.loadResourceImage("clef.png"));
		setResizable(true);
		setVisible(true);
		setLocationRelativeTo(null);
	}

	public static void main(String args[]) {
		PersonenPanelFrame frame = new PersonenPanelFrame();
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(1);
			}
		});
	}
}