import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.sound.sampled.Clip;
import javax.swing.JPanel;

import com.sun.media.jfxmedia.AudioClip;
import com.sun.prism.paint.Paint;

class Person extends JPanel implements MouseListener {

	public String name = "", vorname = "";
	public boolean paintFlag = false;
	public Image bild;
	public Clip audioClip;

	public Person(String name, String vorname, String bild, String audioClip) {

		this.name = name;
		this.vorname = vorname;
		// this.paintFlag = paintFlag;
		this.bild = Utility.loadResourceImage(bild);
		this.audioClip = Utility.loadAudioClip(audioClip);

		addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		this.paintFlag = true;
		//audioClip.setFramePosition(0);
		audioClip.start();
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		this.paintFlag = false;
		audioClip.stop();
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);

		Color bg = paintFlag ? Color.BLUE : Color.LIGHT_GRAY;
		g.setColor(bg);
		g.fillRect(5, 5, getWidth() - 5, getHeight() - 5);
		g.setColor(Color.BLACK);
		g.drawRect(5, 5, getWidth() - 5, getHeight() - 5);
		g.drawImage(bild, 20, 50, getWidth() - 40, getHeight() - 30, null);
		g.drawString(this.name, 20, 40);
		g.drawString(this.vorname, 20, 20);
	}

}
